#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'edscraper'
SITENAME = "EdScraper' log"
SITESUBTITLE = 'A personal website'
SITEURL = 'https://edscraper.gitlab.io'

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'America/Belem'

DEFAULT_LANG = 'pt-br'

# Themes and plugins
THEME = 'pure-single'
TAGLINE = 'Data Science'
PROFILE_IMG_URL= '/images/logo.jpg'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
#CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

STATIC_PATHS = ['images']

# Menu
MENUITEMS = (('About', '/pages/about.html'),
        ('Archives', '/pages/archives.html'),
        ('Categories', '/pages/categories.html'),)
        
DISPLAY_PAGES_ON_MENU = True

TEMPLATE_PAGES = {
        '../pure-single/templates/archives.html' : 'pages/archives.html',
        '../pure-single/templates/categories.html' : 'pages/categories.html'
        }

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),)

# Social widget
SOCIAL = (('twitter', 'https://twitter.com/earaujopy'),
          ('gitlab', 'https://gitlab.com/edscraper'),
          ('at', 'mailto:edscraper@gmail.com'),
          ('rss', '/feeds/all.atom.xml'))

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
