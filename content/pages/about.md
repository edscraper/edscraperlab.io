Title: About
Date: 2019-01-28 10:55


26 anos, graduado em bach. em sistemas de informação e atualmente é doutorando em biotecnologia.

O interesse em ciência de dados surgiu, primeiramente, com a raspagem de dados por influência de um vídeo no youtube do Professor Fernando Masanori. Segue o link: [Hackeando dados públicos com Python 3 - Fernando Masanori](https://www.youtube.com/watch?v=pM68J2JA72U)

O primeiro projeto teve o objetivo de acessar dados do Portal da Transparência do Governo Federal. Nele os dados de gastos de 7 ministérios no período de 2004 a 2018 foram coletados, tratados e apresentados em gráficos.

Depois disso, foi a vez de raspar os gastos do cartão corporativo do Estado do Ceará. O período dos dados coletados no portal da transparência desse Estado foi de 2015 a 2018. E desta vez, além dos gráficos, foram realizados cálculos da média e de desvio padrão anual dos gastos em questão.

Agora é pretendido voltar e aprofundar nos estudos acerca de Data Science e torná-la a minha profissão. :)
